package controllers

import (
	"encoding/json"
	"fmt"
	ssoUserModel "gwv8/app/sso/models"
	"gwv8/config"
	model "gwv8/system"
	"gwv8/utils"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

var db = config.ConnectDB()

// ErrorResponse error response
type ErrorResponse struct {
	Err string
}

// CreateUser create user
func CreateUser(w http.ResponseWriter, r *http.Request) {
	ssoUser := &ssoUserModel.SsoUser{}
	err := json.NewDecoder(r.Body).Decode(ssoUser)
	if err != nil {
		log.Fatal(err)
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(ssoUser.UserPassword), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
		err := ErrorResponse{
			Err: "Password Encryption Failed",
		}
		json.NewEncoder(w).Encode(err)
	}

	ssoUser.UserPassword = string(pass)

	createdUser := db.Create(ssoUser)
	var errMessage = createdUser.Error

	if createdUser.Error != nil {
		fmt.Println(errMessage)
	}
	json.NewEncoder(w).Encode(createdUser)
}

// GetAllUsers for get all users
func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	var ssoUser []ssoUserModel.SsoUser
	params := mux.Vars(r)
	result := model.Response{}

	maxPerPage, _ := strconv.Atoi(params["maxperpage"])
	setPage, _ := strconv.Atoi(params["setpage"])

	q := db.Model(&ssoUser)
	paging := utils.Pagination(q, maxPerPage, setPage, &ssoUser)

	result.Status = 1
	result.Message = "Success"
	result.Data = ssoUser
	result.PagingData = append(result.PagingData, paging)

	json.NewEncoder(w).Encode(result)
}

// GetUserDataTable data
func GetUserDataTable(w http.ResponseWriter, r *http.Request) {
	var ssoUser []ssoUserModel.SsoUser
	params := mux.Vars(r)
	maxPerPage, _ := strconv.Atoi(params["maxperpage"])
	setPage, _ := strconv.Atoi(params["setpage"])

	q := db.Model(&ssoUser)

	dataTable := utils.DataTable(q, maxPerPage, setPage, &ssoUser, true, "user_full_name ASC")

	json.NewEncoder(w).Encode(dataTable)
}
