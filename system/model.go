package system

// Exception for Exception
type Exception struct {
	Message string `json:"message"`
}

// Response default
type Response struct {
	Status     int    `json:"status"`
	Message    string `json:"message"`
	Data       interface{}
	PagingData []interface{}
}

// DataTable type response
type DataTable struct {
	Status     int    `json:"status"`
	Message    string `json:"message"`
	Data       interface{}
	PagingData []interface{}
	Search     bool
	OrderBy    string `json:"orderby"`
}
