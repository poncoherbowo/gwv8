package main

import (
	"log"
	"net/http"
	"os"

	Routes "gwv8/routes"

	"github.com/joho/godotenv"
)

func main() {
	e := godotenv.Load()

	if e != nil {
		log.Fatal("Error loading .env file")
	}

	port := os.Getenv("PORT")
	// config.MigrateSchema()
	// Handle routes
	router := Routes.Handlers()

	// serve
	log.Printf("Server up on port '%s'", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
