package utils

import (
	"github.com/jinzhu/gorm"
	"github.com/vcraescu/go-paginator"
	"github.com/vcraescu/go-paginator/adapter"
	"github.com/vcraescu/go-paginator/view"
)

// Paging type struct
type Paging struct {
	Pages   []int `json:"pages"`
	Next    int   `json:"next"`
	Prev    int   `json:"prev"`
	Current int   `json:"current"`
}

// Pagination function
func Pagination(d *gorm.DB, maxPerPage int, setPage int, model interface{}) interface{} {
	p := paginator.New(adapter.NewGORMAdapter(d), maxPerPage)
	p.SetPage(setPage)
	p.Results(model)

	paging := Paging{}

	view := view.New(&p)

	paging.Pages = view.Pages()
	paging.Next = view.Next()
	paging.Prev = view.Prev()
	paging.Current = view.Current()

	return paging
}
