package config

import (
	"fmt"
	"log"
	"os"

	ssoModels "gwv8/app/sso/models"

	"github.com/jinzhu/gorm"
	// _ "github.com/jinzhu/gorm/dialects/mysql" //Gorm mysql dialect interface
	_ "github.com/jinzhu/gorm/dialects/postgres" //Gorm postgres dialect interface
	"github.com/joho/godotenv"
)

//ConnectDB function: Make database connection
func ConnectDB() *gorm.DB {

	//Load environmenatal variables
	err := godotenv.Load()
	var dbURI string

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	databaseUser := os.Getenv("databaseUser")
	databasePassword := os.Getenv("databasePassword")
	databaseName := os.Getenv("databaseName")
	databaseType := os.Getenv("databaseType")
	databaseHost := os.Getenv("databaseHost")
	databasePort := os.Getenv("databasePort")

	//Define DB connection string
	if databaseType == "mysql" {
		dbURI = fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", databaseUser, databasePassword, databaseName)
	} else if databaseType == "postgres" {
		dbURI = fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", databaseHost, databasePort, databaseUser, databaseName, databasePassword)
	}

	//connect to db URI
	db, err := gorm.Open(databaseType, dbURI)

	if err != nil {
		fmt.Println("error", err)
		panic(err)
	}
	// close db when not in use
	// defer db.Close()

	fmt.Println("Successfully connected!")
	return db
}

// MigrateSchema funciton: Migrate all schema
func MigrateSchema() {
	db := ConnectDB()

	// Migrate the schema
	db.Debug().AutoMigrate(
		&ssoModels.SsoAccess{},
		&ssoModels.SsoApplication{},
		&ssoModels.SsoGroupAccess{},
		&ssoModels.SsoGroup{},
		&ssoModels.SsoMenu{},
		&ssoModels.SsoModule{},
		&ssoModels.SsoUserApplication{},
		&ssoModels.SsoUserGroup{},
		&ssoModels.SsoUser{},
	)
}
