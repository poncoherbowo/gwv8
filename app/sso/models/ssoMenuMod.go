package models

import "time"

// SsoMenu type for sso_users table
type SsoMenu struct {
	MenuID       uint `gorm:"primary_key;AUTO_INCREMENT"`
	MenuParentID uint
	ModuleID     string `gorm:"type:varchar(60)"`
	MenuName     string `gorm:"type:varchar(100)"`
	MenuIcon     string `gorm:"type:varchar(100)"`
	MenuLink     string `gorm:"type:varchar(1000)"`
	MenuActive   uint
	MenuOrder    string `gorm:"type:varchar(60)"`
	CreateUser   string
	CreatedAt    time.Time
	UpdateUser   string
	UpdatedAt    time.Time
	DeleteUser   string
	DeletedAt    *time.Time
}
