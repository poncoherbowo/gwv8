package controllers

import (
	"encoding/json"
	"fmt"
	ssoUserModel "gwv8/app/sso/models"
	"gwv8/utils"

	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

// HomepageIndex homepage
func HomepageIndex(w http.ResponseWriter, r *http.Request) {
	post := "Welcome"
	json.NewEncoder(w).Encode(post)
}

// Login function
func Login(w http.ResponseWriter, r *http.Request) {
	user := &ssoUserModel.SsoUser{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		var resp = map[string]interface{}{"status": false, "message": "Invalid request"}
		json.NewEncoder(w).Encode(resp)
		return
	}
	resp := FindOne(user.UserEmail, user.UserPassword)
	json.NewEncoder(w).Encode(resp)
}

// FindOne find one user
func FindOne(email, password string) map[string]interface{} {
	user := &ssoUserModel.SsoUser{}

	if err := db.Where("user_email = ?", email).First(user).Error; err != nil {
		var resp = map[string]interface{}{"status": false, "message": "Email address not found"}
		return resp
	}
	expiresAt := time.Now().Add(time.Minute * 50).Unix()

	errf := bcrypt.CompareHashAndPassword([]byte(user.UserPassword), []byte(password))
	if errf != nil && errf == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
		var resp = map[string]interface{}{"status": false, "message": "Invalid login credentials. Please try again"}
		return resp
	}

	tk := &utils.Token{
		UserID: user.UserID,
		Name:   user.UserFullName,
		Email:  user.UserEmail,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}

	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, error := token.SignedString([]byte("secret"))
	if error != nil {
		fmt.Println(error)
	}

	var resp = map[string]interface{}{"status": false, "message": "logged in"}
	resp["token"] = tokenString //Store the token in the response
	resp["user"] = user
	return resp
}
