package models

import "time"

// SsoAccess type for sso_accesss table
type SsoAccess struct {
	AccessID   string `gorm:"primary_key;type:varchar(5)"`
	AccessName string `gorm:"type:varchar(60)"`
	CreateUser string
	CreatedAt  time.Time
	UpdateUser string
	UpdatedAt  time.Time
	DeleteUser string
	DeletedAt  *time.Time
}
