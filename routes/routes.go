package routes

import (
	homeController "gwv8/app/sso/controllers"
	ssoUserController "gwv8/app/sso/controllers"
	"net/http"
	"os"
)

// Route type
type Route struct {
	Name        string
	Method      string
	Pattern     string
	Queries     []string
	HandlerFunc http.HandlerFunc
}

// Routes type
type Routes []Route

var vers = os.Getenv("API_VERSION")
var baseURL = "/api/" + vers + "/"

// Noauth routes
var routes = Routes{
	Route{
		"HomepageIndex",
		"GET",
		baseURL,
		[]string{"", ""},
		homeController.HomepageIndex,
	},
	Route{
		"Login",
		"POST",
		baseURL + "login",
		[]string{"", ""},
		homeController.Login,
	},
	// Route{
	// 	"CreateUser",
	// 	"POST",
	// 	baseURL + "users",
	// 	[]string{"", ""},
	// 	ssoUserController.CreateUser,
	// },
}

// Routes with jwt auth
var authRoutes = Routes{
	// Route{
	// 	"GetAllUsers",
	// 	"GET",
	// 	baseURL + "users",
	// 	[]string{"maxperpage", "{maxperpage}", "setpage", "{setpage}"},
	// 	// []string{"", ""},
	// 	ssoUserController.GetAllUsers,
	// },
	Route{
		"GetUserDataTable",
		"GET",
		baseURL + "users",
		// []string{"maxperpage", "{maxperpage}", "setpage", "{setpage}", "search", "{search}", "ob", "{ob}"},
		[]string{"maxperpage", "{maxperpage}", "setpage", "{setpage}"},
		ssoUserController.GetUserDataTable,
	},
	Route{
		"CreateUser",
		"POST",
		baseURL + "users",
		[]string{"", ""},
		ssoUserController.CreateUser,
	},
}
