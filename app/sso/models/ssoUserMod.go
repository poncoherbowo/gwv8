package models

import "time"

// SsoUser type for sso_users table
type SsoUser struct {
	UserID               uint   `gorm:"primary_key;AUTO_INCREMENT"`
	UserName             string `gorm:"unique;not null"`
	UserEmpID            string `gorm:"unique;not null"`
	UserFullName         string
	UserEmail            string `gorm:"unique;not null"`
	UserPassword         string
	UserLastActivityTime string
	UserIsLogin          int
	UserStatus           int
	UserCountWrongPass   int
	CreateUser           string
	CreatedAt            time.Time
	UpdateUser           string
	UpdatedAt            time.Time
	DeleteUser           string
	DeletedAt            *time.Time
}
