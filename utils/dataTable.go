package utils

import (
	"gwv8/system"

	"github.com/jinzhu/gorm"
)

// DataTable for get data for generate table
func DataTable(q *gorm.DB, maxPerPage int, setPage int, model interface{}, search bool, orderby string) interface{} {
	result := system.DataTable{}

	paging := Pagination(q, maxPerPage, setPage, model)

	result.Status = 1
	result.Message = "Success"
	result.Data = model
	result.PagingData = append(result.PagingData, paging)
	result.Search = search
	result.OrderBy = orderby

	return result
}
