package models

import "time"

//SsoUserApplication type for sso_application table
type SsoUserApplication struct {
	UserID        string `gorm:"primary_key;type:varchar(60)"`
	ApplicationID string `gorm:"type:varchar(60)"`
	CompanyID     string `gorm:"type:varchar(10)"`
	BranchID      string `gorm:"type:varchar(20)"`
	CreateUser    string
	CreatedAt     time.Time
	UpdateUser    string
	UpdatedAt     time.Time
	DeleteUser    string
	DeletedAt     *time.Time
}
