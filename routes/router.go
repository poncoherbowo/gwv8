package routes

import (
	"gwv8/utils"
	"net/http"

	"github.com/gorilla/mux"
)

// Handlers handle all endpoint
func Handlers() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	r.Use(CommonMiddleware)

	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		r.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	s := r.PathPrefix("/auth").Subrouter()
	s.Use(utils.JwtVerify)
	s.Use(CommonMiddleware)

	for _, authRoute := range authRoutes {
		var handler http.Handler

		handler = authRoute.HandlerFunc
		handler = Logger(handler, authRoute.Name)
		s.
			Methods(authRoute.Method).
			Path(authRoute.Pattern).
			Queries(authRoute.Queries...).
			Name(authRoute.Name).
			Handler(handler)

	}

	return r
}

// CommonMiddleware --Set content-type
func CommonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Access-Control-Request-Headers, Access-Control-Request-Method, Connection, Host, Origin, User-Agent, Referer, Cache-Control, X-header")
		next.ServeHTTP(w, r)
	})
}
