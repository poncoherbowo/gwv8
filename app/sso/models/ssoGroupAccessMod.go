package models

import "time"

//SsoGroupAccess type for sso_application table
type SsoGroupAccess struct {
	GroupID    uint   `gorm:"primary_key"`
	ModuleID   string `gorm:"primary_key;type:varchar(100)"`
	GroupAcess string `gorm:"type:text"`
	CreateUser string
	CreatedAt  time.Time
	UpdateUser string
	UpdatedAt  time.Time
	DeleteUser string
	DeletedAt  *time.Time
}
