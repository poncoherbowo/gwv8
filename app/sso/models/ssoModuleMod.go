package models

import "time"

//SsoModule type for sso_application table
type SsoModule struct {
	ModuleID      string `gorm:"primary_key;type:varchar(100)"`
	ApplicationID string `gorm:"primary_key;type:varchar(60)"`
	ModuleName    string `gorm:"type:varchar(120)"`
	CreateUser    string
	CreatedAt     time.Time
	UpdateUser    string
	UpdatedAt     time.Time
	DeleteUser    string
	DeletedAt     *time.Time
}
