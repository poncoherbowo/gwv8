package models

import "time"

//SsoGroup type for sso_groups table
type SsoGroup struct {
	GroupID    uint   `gorm:"primary_key;AUTO_INCREMENT"`
	GrupName   string `gorm:"type:varchar(100);"`
	CreateUser string
	CreatedAt  time.Time
	UpdateUser string
	UpdatedAt  time.Time
	DeleteUser string
	DeletedAt  *time.Time
}
