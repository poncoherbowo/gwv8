package models

import "time"

//SsoUserGroup type for sso_application table
type SsoUserGroup struct {
	UserID     string `gorm:"primary_key;type:varchar(60)"`
	GroupID    uint   `gorm:"primary_key"`
	CreateUser string
	CreatedAt  time.Time
	UpdateUser string
	UpdatedAt  time.Time
	DeleteUser string
	DeletedAt  *time.Time
}
