package models

import "time"

//SsoApplication type for sso_application table
type SsoApplication struct {
	ApplicationID   string `gorm:"primary_key;type:varchar(60)"`
	ApplicationName string `gorm:"type:varchar(100)"`
	CreateUser      string
	CreatedAt       time.Time
	UpdateUser      string
	UpdatedAt       time.Time
	DeleteUser      string
	DeletedAt       *time.Time
}
